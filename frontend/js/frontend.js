(function () {
    $('.loader').addClass('hide');

    $(document).on('submit', '.apply_course', function () {
        // $('.loader').addClass('show');
        // remove prior message which might have accumulated during earlier update
        $('.error-message').each(function () {
            $(this).removeClass('make-visible');
            $(this).html('');
        });

        $('input').each(function () {
            $(this).removeClass('errors');
        });


        // current form under processx
        var current_form = $(this);

        // === Dynamically get all the values of input data
        var request_data = {};

        request_data['_token'] = $(this).find('input[name=_token]').val();


        current_form.find('[name]').each(function () {
            request_data[$(this).attr("name")] = $(this).val();

        });

        if($('#plus-two').is(":checked"))
        {
            request_data['question_one']='plus_two';
        }
        if($('#bachelor').is(":checked"))
        {
            request_data['question_one']='bachelor';
        }
        if($('#others').is(":checked"))
        {
            request_data['question_one']='others';
        }
        if($('#facebook').is(":checked"))
        {
            request_data['question_two']='facebook';
        }
        if($('#friends').is(":checked"))
        {
            request_data['question_two']='friends';
        }
        if($('#others_2').is(":checked"))
        {
            request_data['question_two']='others';
        }
        console.log(request_data);

        $.post(

            $(this).attr('action'),
            request_data,
            function (data) {
                console.log(data);

                if (data.status == 'success') {


                    if(data.url.indexOf("thank-you") > -1){
                        window.location.href = data.url;
                    }else{
                        location.reload();
                    }

                } else if (data.status == 'fails') {
                    for (var key in data.errors) {

                        // skip loop if the property is from prototype
                        if (!data.errors.hasOwnProperty(key)) continue;

                        var error_message = data.errors[key];

                        current_form.find("[name=" + key + "]").addClass('errors');

                        var parent = current_form.find("[name=" + key + "]").parent();
                        parent.find('.error-message').addClass('make-visible').html(error_message);

                        if (data.errors.hasOwnProperty('g-recaptcha-response')) {
                            $('.captcha-box .error-message').addClass('make-visible').html(data.errors['g-recaptcha-response']);
                        }

                    }
                    // $('.loader').addClass('hide');
                    $("body, html").animate({
                       scrollTop: $('.apply-course-form').offset().top
                   }, 600);

                }

            }
        );


        return false;

    });

})();
